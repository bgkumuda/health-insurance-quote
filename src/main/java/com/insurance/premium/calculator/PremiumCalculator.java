package com.insurance.premium.calculator;

import org.springframework.stereotype.Component;

import com.insurance.premium.model.CurrentHealth;
import com.insurance.premium.model.Habit;
import com.insurance.premium.model.Customer;

@Component
public class PremiumCalculator {
	private double basePremium = 5000;
	private static final String MALE = "MALE";
	private static final int THREE = 3;

	public double generatePremium(Customer customer) {
		double premium = basePremium;
		premium = calculatePremiumBasedOnAge(customer, premium);

		if (customer.getGender().equalsIgnoreCase(MALE)) {
			premium = calculatePremium(premium, 2);
		}
		premium = calculatePremium(premium, calculatePremiumPercentageForCurrentHealth(customer.getCurrentHealth()));
		premium = calculatePremium(premium, calculatePremiumPercentageForHabits(customer.getHabit()));

		return premium;

	}

	private double calculatePremiumBasedOnAge(Customer customer, double premium) {
		double percent=1;
		if (customer.getAge() >= 18) {
			if (customer.getAge() <25) {
				premium = premium*1.1;
			} else {
				int quotient = customer.getAge() / 5 - 3;
				for (int i = 1; i <= quotient; i++) {
					if (i <= 4)
						percent = percent * 1.1;
					else
						percent = percent * 1.2;
				}
				premium= premium*percent;
			}
		}
		
		return premium;
	}

	private double calculatePremium(double base, int premiumPercent) {
		if (premiumPercent >= 0)
			base += (base * premiumPercent) / 100;
		else
			base -= (base * premiumPercent) / 100;

		return base;
	}
	
	
	private int calculatePremiumPercentageForHabits(Habit habit){
		int premium=0;
		if (habit.isAlcohol()) {
			premium += THREE;
		}
		if (habit.isDrugs()) {
			premium += THREE;
		}
		if (habit.isSmoking()) {
			premium += THREE;

		}
		if (habit.isDailyExercise()) {
			premium -= THREE;
		}
		
		return premium;
	}
	
	private int calculatePremiumPercentageForCurrentHealth(CurrentHealth currentHealth)
	{
		int premium=0;
		if (currentHealth.isOverWeight()) {
			premium++;
		}
		if (currentHealth.isBloodPressure()) {
			premium++;
		}
		if (currentHealth.isBloodSugar()) {
			premium++;
		}
		if (currentHealth.isHyperTension()) {
			premium++;

		}
		return premium;
	}
}
