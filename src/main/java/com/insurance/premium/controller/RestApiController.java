package com.insurance.premium.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.insurance.premium.model.Customer;
import com.insurance.premium.service.UserService;

@RestController
@RequestMapping("/api")
public class RestApiController {
	public static final Logger logger = LoggerFactory.getLogger(RestApiController.class);

	@Autowired
	private UserService userService;
	
/*http://localhost:8081/api/premium/calculate in POSTMAN*/
	@RequestMapping(value = "/premium/calculate", method = RequestMethod.POST)
	public ResponseEntity<?> createUser(@RequestBody Customer customer) {
		logger.info("Creating User : {}", customer);
		double premium=userService.saveUser(customer);
		return new ResponseEntity<String>("Health Insurance Premium for "+
		customer.getName()+" Rs: "+ Math.round(premium), HttpStatus.CREATED);
	}

}
