package com.insurance.premium.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.insurance.premium.calculator.PremiumCalculator;
import com.insurance.premium.model.Customer;

@Configuration
public class AppConfig {
	
	 @Bean
	 PremiumCalculator premiumGenerator() {
		return new PremiumCalculator();
	    }
	 

	 @Bean
	 Customer customer() {
		return new Customer();
	    }

	
}
