package com.insurance.premium;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuoteGeneratorApp {
	
	public static void main(String[] args) {
		SpringApplication springApp = new SpringApplication(QuoteGeneratorApp.class);
		springApp.run(args);
	}
	
	

}
