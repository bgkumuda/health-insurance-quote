package com.insurance.premium.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.insurance.premium.calculator.PremiumCalculator;
import com.insurance.premium.model.Customer;

@Component
public class UserService {

	@Autowired
	private PremiumCalculator premiumCalculator;

	public double saveUser(Customer customer) {
		return premiumCalculator.generatePremium(customer);
	}

}
