package com.insurance.premium.calculator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import org.junit.Before;
import org.junit.Test;

import com.insurance.premium.model.Customer;
import com.insurance.premium.model.testdata.TestData;
public class PremiumCalculatorTest {

	
	private PremiumCalculator premiumCalculator;
	
	@Before
	public void setUp(){
		premiumCalculator= new PremiumCalculator();
		}
	
	  @Test
	    public void testGeneratePremiumValid (){
		double premium=premiumCalculator.generatePremium(TestData.customerItem());
		assertEquals(6856, Math.round(premium));
	  }
	  
	  @Test
	    public void testGeneratePremiumInValid (){
		Customer customerItem= TestData.customerItem();
		customerItem.setAge(10);
		double premium=premiumCalculator.generatePremium(customerItem);
		assertNotSame(6856, Math.round(premium));
	  }
}
