package com.insurance.premium.model.testdata;

import com.insurance.premium.model.CurrentHealth;
import com.insurance.premium.model.Habit;
import com.insurance.premium.model.Customer;

public class TestData {

	public static Customer customerItem()
	{
		Customer customer= new Customer();
		customer.setName("Norman Gomes");
		customer.setAge(34);
		customer.setGender("male");
		customer.setCurrentHealth(currentHealthItem());
		customer.setHabit(habitItem());
		return customer;
	}
	
	public static CurrentHealth currentHealthItem(){
		CurrentHealth currentHealth= new CurrentHealth();
		currentHealth.setBloodPressure(false);
		currentHealth.setHyperTension(false);
		currentHealth.setBloodSugar(false);
		currentHealth.setOverWeight(true);
		return currentHealth;
	}
	
	public static Habit habitItem(){
		Habit habit= new Habit();
		habit.setAlcohol(true);
		habit.setDailyExercise(true);	
		habit.setDrugs(false);
		habit.setSmoking(false);
		return habit;
	}
}
